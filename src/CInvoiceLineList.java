public class CInvoiceLineList extends CList {
	public CInvoiceLine SearchByCode(int code) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CInvoiceLine) node.m_Element).product.m_Code==code) {
				return (CInvoiceLine) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CInvoiceLine e) {
		super.PushBack(e);
	}
	
	public float getImport() {
		float imp = 0;
		CNode n=m_Start;
		while (n!=null) {
			CInvoiceLine invoiceLine = (CInvoiceLine) n.m_Element;
			imp += invoiceLine.P_quantity * invoiceLine.product.m_Price;
			n=n.m_Next;
		}
		return imp;
	}
}