import java.io.PrintStream;

public class CProductList extends CList {
	public CProduct SearchByName(String name) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Name.equalsIgnoreCase(name)) {
				return (CProduct) node.m_Element;				
			}
			node=node.m_Next;
		}
		return null;
	}
	public CProduct SearchByCode(int code) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Code==code) {
				return (CProduct) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CProduct e) {
		super.PushBack(e);
	}
	
	public void PrintProducts(PrintStream out) {
		CNode node=m_Start;
		String spaces = "                  ";
		String spaces2 = "                    ";
		while (node!=null) {
			CProduct prod = (CProduct) node.m_Element;
			out.println(Integer.toString(prod.m_Code) + spaces.substring(0, spaces.length() - String.valueOf(prod.m_Code).length() ) 
			+ prod.m_Name + spaces2.substring(0, spaces2.length() - String.valueOf(prod.m_Name).length())  
			+ Float.toString(prod.m_Price));
			node=node.m_Next;
		}
		
	}
}
