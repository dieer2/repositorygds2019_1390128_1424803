import java.io.PrintStream;

public class CClientList extends CList {
	public CClient SearchByName(String name) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CClient) node.m_Element).m_Name.equalsIgnoreCase(name)) {
				return (CClient) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public CClient SearchByNumber(int number) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CClient) node.m_Element).m_Number==number) {
				return (CClient) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CClient e) {
		super.PushBack(e);
	}
	
	public void printClients(PrintStream out) {
		CNode node=m_Start;
		String spaces = "                    ";
		while (node!=null) {
			CClient client = (CClient) node.m_Element;
			out.println(client.m_Number + spaces.substring(0, spaces.length() - String.valueOf(client.m_Number).length()) 
			+ client.m_Name + spaces.substring(0, spaces.length() - String.valueOf(client.m_Name).length()));
			node = node.m_Next;
		}
		
	}
}
