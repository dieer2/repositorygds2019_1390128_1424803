import java.io.PrintStream;

public class CInvoice extends CElement {
	public int m_Number;
	public CClient m_Client;
	public CInvoiceLineList m_Products;
	public CInvoice(int number,CClient client) {
		m_Number=number;
		m_Client=client;
		m_Products= new CInvoiceLineList();
	}
	public void AddProduct(CInvoiceLine invoiceLine) {
		m_Products.PushBack(invoiceLine);
	}
	public void DeleteProduct(CInvoiceLine invoiceLine) {
		m_Products.Delete(invoiceLine);
	}
	public void Print(PrintStream out) {
		out.print("Invoice(");
		out.print(m_Number);
		out.print(",");
		out.print(m_Client.m_Name);
		out.print(",");
		m_Products.Print(out);
		out.print(")");
	}
	
	public void PrintInvoice(PrintStream out) {
		out.println(m_Number + "                   " + m_Client.m_Name + "                " + Float.toString(m_Products.getImport()));
		
	}
}
