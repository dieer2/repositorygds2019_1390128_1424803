import java.io.PrintStream;

public class CInvoiceLine extends CElement {
	public CProduct product;
	public int P_quantity;
	
	public CInvoiceLine(CProduct prod, int quantity) {
		this.product = prod;
		this.P_quantity = quantity;
	}
	
	public void Print(PrintStream out) {
		out.print("CInvoiceLine(");
		out.print(product.m_Name);
		out.print(",");
		out.print(P_quantity);
		out.print(")");
	}
}